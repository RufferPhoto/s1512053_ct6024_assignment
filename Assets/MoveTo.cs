﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveTo : MonoBehaviour {
    GameObject goDestination;
    NavMeshAgent xNavmeshAgent;

	// Use this for initialization
	void Start ()
    {
        // Reference to Destination
        goDestination = GameObject.Find("Destination");
        // Reference to navmesh component
        xNavmeshAgent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        // Creating a vector3 from the position of the destination.
        Vector3 v3TargetPos = goDestination.transform.position;
        // Telling the navmesh to go to the v3 position of destination.
        xNavmeshAgent.SetDestination(v3TargetPos);
		
	}
}
