﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Using a namespace so that the scripts can be modular.
// Which means they can be put into other projects without having to link anything.
namespace DG.AI
{
    public class AIDirector : MonoBehaviour
    {
        // List of AI stats.
        public List<EnemyAi> allEnemies = new List<EnemyAi>();
        // Putting the cover position's in a list.
        public List<Transform> coverPositions = new List<Transform>();
        // Spy Hide positions.
        public List<Transform> hidePositions = new List<Transform>();
        // Singleton logic.
        public static AIDirector singleton;

        // This function returns the closest cover position from the AI.
        // Important -- Cover gameObjects need to be facing the correct way in
        // order for the AI to recognise them as valid cover points.
        // otherwise AI will switch into a chase state.
        public Transform GetCover(Vector3 from, Vector3 towards)
        {
            Transform r = null;
            // Minimum distance.
            float minDist = Mathf.Infinity;

            for(int i = 0; i < coverPositions.Count; i++)
            {
                // Distance between the AI and Cover.
                float distanceFrom = Vector3.Distance(from, coverPositions[i].position);

                // Distance between the AI and the Spy/Target.
                float distanceFromTarget = Vector3.Distance(from, towards);

                // Direction of cover.
                Vector3 direction = towards - coverPositions[i].position;


                // Skips the following if the cover is too far away from the AI, or the distance 
                // between the AI and the cover is greater than the distance between the AI and the Spy/Target.
                // In other words it will skip if the cover is on the otherside of the player.
                if (distanceFrom > 80 || distanceFromTarget < distanceFrom)
                    continue;

                // Angle towards the enemy from that current position.
                float angle = Vector3.Angle(direction, coverPositions[i].forward);
                direction.y = 0;
                
                if (angle < 75)
                {
                    // Getting the distance and assigning it to a temp variable.
                    float tempDist = Vector3.Distance(from, coverPositions[i].position);

                    // Closest cover position.
                    if (tempDist < minDist)
                    {
                        minDist = tempDist;
                        r = coverPositions[i];
                    }
                }
            }
            return r;
        }

        public Transform GetHide(Vector3 from, Vector3 towards)
        {
            Transform r = null;
            // Minimum distance.
            float minDist = Mathf.Infinity;

            for (int i = 0; i < hidePositions.Count; i++)
            {
                // Distance between the AI and Cover.
                float distanceFrom = Vector3.Distance(from, hidePositions[i].position);

                // Distance between the AI and the Spy/Target.
                float distanceFromTarget = Vector3.Distance(from, towards);

                // Direction of cover.
                Vector3 direction = towards - hidePositions[i].position;

                
                if (distanceFrom > 80)
                {
                    Debug.Log("skip hide distance is above 80");
                    continue;
                }
                // Angle towards the enemy from that current position.
                float angle = Vector3.Angle(direction, hidePositions[i].forward);
                direction.y = 0;
                
                // Getting the distance and assigning it to a temp variable.
                float tempDist = Vector3.Distance(from, hidePositions[i].position);

                // Closest cover position.
                if (tempDist < minDist)
                {
                    minDist = tempDist;
                    r = hidePositions[i];
                }
                
            }
            return r;
        }

        public void AlertCloseEnemies(EnemyAi ai, Vector3 from)
        {
            for (int i = 0; i < allEnemies.Count; i++)
            {

                // Check to see if it's not the AI calling this method.
                // This is because the AI is already in this state.
                if (allEnemies[i] == ai)
                    continue;

                if (allEnemies[i].aiState == EnemyAi.AIState.inView)
                    continue;

                allEnemies[i].ChangeState(EnemyAi.AIState.inView);
            }
        }

        void Awake()
        {
            singleton = this;
        }

    }
}
