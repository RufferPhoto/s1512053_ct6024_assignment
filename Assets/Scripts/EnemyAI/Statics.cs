﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DG.AI
{
    public static class Statics
    {
        public static int RandToHundred()
        {
            return Random.Range(0, 101);
        }
    }
}
