﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// S1512053 Dan Gregg CT6024 Advanced AI Algorithms
// EnemyAI class.

namespace DG.AI
{
    public class EnemyAi : MonoBehaviour
    {
        // Target of spy AI.
        public Transform spyTarget;
        Vector3 direction;
        Vector3 rotDirection;
        // Reference to Waypoints
        public Vector3[] WPRotDirection;



        // Field of view angle of AI.
        public float FOVAngle;
        // Radius
        public float radius = 10;
        // Speed of enemy
        public float speed = 5;
        // Max distance before going into lateBehaviour to reduce uneccessary CPU usage.
        public float maxDistance = 30;



        // Delayed frame / Late behaviour counter.
        // Optimisation so that some functions only run on certain frames and not everyframe.
        int i_lateFrame = 10;
        int i_lateFrameCounter = 0;

        int i_llateFrame = 35;
        int i_llateFrameCounter = 0;

        // Private
        // This prevents physics running when not needed. i.e when spy is on other side of the map,
        // without this unity's physics engine will still be called and increase cpu/gpu usage.
        public bool _isInView;
        public bool _isInAngle;
        public bool _isClear;

        public AIState aiState;
        public AIState targetState;
        public inViewSubCat inViewSub;

        // Delegates to be used as a variable to be passed around functions or states.
        delegate void EveryFrame();
        EveryFrame everyFrame;
        delegate void LateFrame();
        LateFrame lateFrame;
        delegate void LateLateFrame();
        LateLateFrame llateFrame;

        // Reference to navmesh agent.
        [HideInInspector]
        public NavMeshAgent agent;
        // Instantiating Enemy Stats for each Enemy with this script attached.
        public EnemyStats enemyStats = new EnemyStats();

        // Debugging 
        public bool changeState;
       

        void Start()
        {
            agent = GetComponent<NavMeshAgent>();
            aiState = AIState.idle;
            ChangeState(AIState.idle);
            // Adding enemy AI to the AIDirector.
            AIDirector.singleton.allEnemies.Add(this);
        }

        void Update()
        {
            if(changeState) // Debugging.
            {
                ChangeState(targetState);
                changeState = false;
            }
            MonitorStates();

            // Runs everyframe
            if (everyFrame != null)
                everyFrame();
            i_lateFrameCounter++;

            if (i_lateFrameCounter > i_lateFrame)
            {
                if (lateFrame != null)
                {
                    lateFrame();
                }
                
                i_lateFrameCounter = 0;
            }

            i_llateFrameCounter++;
            if (i_llateFrameCounter > i_llateFrame)
            {
                if (llateFrame != null)

                    llateFrame();
                i_llateFrameCounter = 0;
            }
        }

        void MonitorStates() // Runs on everyframe.
        {
            switch (aiState)
            {
                case AIState.idle:
                    // If spy/player comes within radius change state to inRadius.
                    if (enemyStats.distanceFromTarget < radius)
                        ChangeState(AIState.inRadius);
                    // If spy/player moves further than maxdistance away from enemy change state to slower update (lateIdle).
                    if (enemyStats.distanceFromTarget > maxDistance)
                        ChangeState(AIState.lateIdle);
                    break;
                case AIState.lateIdle:
                    // If spy/player comes closer than maxdistance for late idle then return to idle, which updates faster.
                    if (enemyStats.distanceFromTarget < maxDistance)
                        ChangeState(AIState.idle);
                    break;
                case AIState.inRadius:
                    // If distance is higher than radius return to idle.
                    if (enemyStats.distanceFromTarget > radius)
                        ChangeState(AIState.idle);
                    // If spy/player is in view change state to inView.
                    if (_isClear)
                        ChangeState(AIState.inView);
                    break;
                case AIState.inView:
                    if (enemyStats.distanceFromTarget > radius)
                        ChangeState(AIState.idle);

                    // Can be an issue here if the angular speed is too low.
                    // Because the AI will not turn fast enough to get the target inAngle
                    // This means that the AI will not reaquire the target even if the target hasn't moved. 
                    if (!_isClear)
                    {
                        Debug.Log("Target Lost");
                        ChangeState(AIState.inSearch);
                    }
                    break;
                case AIState.inSearch:
                    if (_isClear)
                        ChangeState(AIState.inView);
                    MonitorBehaviourLife();
                    break;
                default:
                    break;
            }
        }


        void MonitorBehaviourLife()
        {
            enemyStats.behaviourLife += Time.deltaTime;
            if (enemyStats.behaviourLife > enemyStats.maxBehaviourLife)
            {
                enemyStats.behaviourLife = 0;
                ChangeState(targetState);
            }
        }


        public void ChangeState(AIState targetState)
        {
            aiState = targetState;
            // Empty all the delegates when change state occurs so that the 
            // AI doesn't run the previous set state.
            everyFrame = null;
            lateFrame = null;
            llateFrame = null;

            switch (targetState)
            {
                // Distance check needs to occur in each state.
                case AIState.idle:
                    lateFrame = IdleBehaviour;
                    break;
                case AIState.lateIdle:
                    llateFrame = IdleBehaviour;
                    break;
                case AIState.inRadius:
                    // Assigning the InRadiusBehaviour to the delegate.
                    lateFrame = InRadiusBehaviour;
                    break;
                case AIState.inView:
                    lateFrame = InViewBehaviourSec;
                    everyFrame = InViewBehaviour;
                    // Update last known position.
                    enemyStats.lastKnownPosition = spyTarget.position;
                    StopMoving();

                    int randValue = Statics.RandToHundred();
                    

                    // Creating randomness for the AI. 90% chance of going to cover. 10% chance of going chasing the target.
                    if(randValue < 90)
                    {
                        inViewSub = inViewSubCat.lookForCover;
                    }
                    else
                    {
                        inViewSub = inViewSubCat.inChase;
                    }

                    if(Vector3.Distance(transform.position, enemyStats.lastKnownPosition) < 5)
                    {
                        inViewSub = inViewSubCat.inChase;
                    }

                    AIDirector.singleton.AlertCloseEnemies(this, transform.position);
                    
                    // Move to last known position.
                    break;
                case AIState.inSearch:
                    lateFrame = InSearchBehaviour;
                    enemyStats.lastKnownPosition = spyTarget.position;
                    // Updating candidate position.
                    enemyStats.candidatePosition = enemyStats.lastKnownPosition;
                    // Move to position.
                    MoveToPosition(enemyStats.lastKnownPosition);
                    // Updating max behaviourlife.
                    float bhlifeOffset = Random.Range(-2, 3);
                    enemyStats.maxBehaviourLife = CommonBehaviours.searchLife + bhlifeOffset;
                    targetState = AIState.idle;
                    WaypointsBase wp = new WaypointsBase();
                    enemyStats.currentWaypoint = wp;
                    break;
                default:
                    break;
            }
        }

        // Encapsulating distance check for consistency
        void IdleBehaviour()
        {
            if (spyTarget == null)
                return;
            // Rotating towards waypoint - may be used in another behaviour i.e. patrol or search.
            //RotateTowardsWayPoint();
            DistanceCheckPlayer(spyTarget);
            CommonBehaviours.PatrolBehaviour(this);
        }

        // Encapsulating the behaviours when the spy or target is within the radius.
        void InRadiusBehaviour()
        {
            if (spyTarget == null)
                return;

            Sight();
            CommonBehaviours.PatrolBehaviour(this);
        }

        void InViewBehaviour()
        {
            if (spyTarget == null)
                return;

            FindDirection(spyTarget);
            DistanceCheckPlayer(spyTarget);

            switch(inViewSub)
            {
                case inViewSubCat.inRange:
                    RotateTowardsTarget();
                    break;
                case inViewSubCat.inChase:
                    CommonBehaviours.ChaseBehaviour(this);
                    break;
                case inViewSubCat.inCover:
                    RotateTowardsTarget();
                    break;
                case inViewSubCat.lookForCover:
                    CommonBehaviours.GetToCover(this);
                    break;
                default:
                    break;
            }         
        }

        void InViewBehaviourSec()
        {
            Sight();
            MonitorTargetPosition();
        }

        // Function is called when the AI is inView of the target but needs a behaviour i.e. chase the Spy.
        public void ChangeInViewState(inViewSubCat target)
        {
            inViewSub = target;
        }

        void InSearchBehaviour()
        {
            // Calling the behaviour from common behaviours.
            CommonBehaviours.SearchBehaviour(this);
            Sight();
        }

        // Encapsulating the behaviour so that the bahviour doesn't need to be written twice.
        void Sight()
        {
            // Find distance first.
            DistanceCheckPlayer(spyTarget);
            // Find the direction.
            FindDirection(spyTarget);
            // From the direction the angle can then be calculated.
            AngleCheck();
            // Are there any objects in the way of the view.
            if(_isInAngle)
                IsClearView(spyTarget);
        }
         
        void DistanceCheckPlayer(Transform target)
        {
            enemyStats.distanceFromTarget = Vector3.Distance(transform.position, target.position);
        }

        void IsClearView(Transform target)
        {
            // Everytime this isClear is false until the raycast hits the player.
            _isClear = false;
            // Raycasting to check whether the player is behind an object.
            RaycastHit hit;
            // Origin of Enemy AI.
            Vector3 origin = transform.position;
            
            // Drawing line to the target.
            Debug.DrawRay(origin, direction * radius);

            // If hit
            if (Physics.Raycast(origin, direction, out hit, radius))
            {
                if (hit.transform.CompareTag("Spy"))
                {
                    Debug.Log("hit player");
                    _isClear = true;
                }
            }
        }

        void AngleCheck()
        {
            Vector3 rotDirection = direction;
            // Forcing y to = 0.
            rotDirection.y = 0;

            // Default if direction position is a null vector.
            if (rotDirection == Vector3.zero)
                rotDirection = transform.forward;

            // Calculating whether or not the spy is within AI Field of View.
            float f_angle = Vector3.Angle(transform.forward, rotDirection);

            // Changes the bool to true if player is within the field of view.
            _isInAngle = (f_angle < FOVAngle);
        }

        void RotateTowardsTarget()
        {
            if (rotDirection == Vector3.zero)
                rotDirection = transform.forward;

            // Calculating rotation to target.
            Quaternion targetRotation = Quaternion.LookRotation(rotDirection);
            // Applying
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 5);
        }

        void RotateTowardsWayPoint()
        {
            // Calculating rotation to target.
            Quaternion targetRotation = Quaternion.LookRotation(WPRotDirection[0]);
            // Applying
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 5);
        }

        void FindDirection(Transform target)
        {
            direction = target.position - transform.position;
            rotDirection = direction;
            rotDirection.y = 0;
        }

        void MonitorTargetPosition()
        {
            float delta_distance = Vector3.Distance(spyTarget.position, enemyStats.lastKnownPosition);
            if (delta_distance > 2)
            {
                enemyStats.lastKnownPosition = spyTarget.position;
                MoveToPosition(enemyStats.lastKnownPosition);
            }
        }

        // Using navmeshagent to move to position.
        // Note that this should only be called once.
        // So that it doesn't recreate the path everyframe.
        public void MoveToPosition(Vector3 targetPos)
        {
            agent.isStopped = false;
            // Function could be used to move to last known position of SPY.
            // But keeping it as passed in target means the enemy can move to any position.
            agent.SetDestination(targetPos);
        }

        public void StopMoving()
        {
            agent.isStopped = true;
        }

        // Function to give a random position around a given target. I.e. producing a position to search.
        // Target position will be the last known position of the target.
        public Vector3 RandomVector3AroundTarget(Vector3 targetPosition)
        {
            // Offset to be used with the targetPosition.
            float offsetX = Random.Range(-3, 3);
            float offsetZ = Random.Range(-3, 3);
            // Origin of target.
            Vector3 orignPos = targetPosition;
            // Adding the random offsets.
            orignPos.x += offsetX;
            orignPos.z += offsetZ;

            // Reference to NavMesh.
            NavMeshHit hit;

            // Hit.
            if (NavMesh.SamplePosition(orignPos, out hit, 5, NavMesh.AllAreas))
            {
                return hit.position;
            }

            // Return a vector 3 target position.
            return targetPosition;
        }

        public enum inViewSubCat
        {
            inRange, inChase, inCover, lookForCover
        }

        public enum AIState
        {
            idle, lateIdle, inRadius, inView, inSearch
        }
    }
}
