﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace DG.AI
{
    // Ability to control the AI from this class.
    public static class CommonBehaviours
    {
       
        // 
        public static float searchLife = 7;

        public static void PatrolBehaviour(EnemyAi ai)
        {
            EnemyStats e = ai.enemyStats;
            // If waypoints exist.
            if(e.waypoints.Count > 0)
            {
                if (e.currentWaypoint.targetDestination == null)
                {
                    e.currentWaypoint = e.waypoints[e.waypointIndex];
                    ai.MoveToPosition(e.currentWaypoint.targetDestination.position);
                }
                if (ai.agent.velocity == Vector3.zero)
                {
                    ai.agent.isStopped = false;
                }
                // Go to waypoint.
                e.currentWaypoint = e.waypoints[e.waypointIndex];
                Debug.Log("Current Enemy Waypoint" + e.currentWaypoint);
                // Calculating the waypoint distance.
                e.waypointDistance = Vector3.Distance(ai.transform.position, e.currentWaypoint.targetDestination.position);

                if (e.waypointDistance < 2)
                {
                    e.waitTimeWP += Time.deltaTime * 10; // * 15 cause called on every 15 frames, so offset of 15 to bring it to realtime.

                    if (e.waitTimeWP > e.maxWaitTime)
                    {
                        e.waitTimeWP = 0;
                        // Go to next waypoint or if there isn't another one reverse.
                        if (e.waypointIndex < e.waypoints.Count - 1)
                        {
                            // Increment the index to move to the next waypoint in array.
                            e.waypointIndex++;
                        }
                        else
                        {
                            // Runs when waypointIndex reaches the end.
                            // And then reverses the array to create a never ending loop.
                            e.waypointIndex = 0;
                            e.waypoints.Reverse();
                        }

                        // Updating the current waypoint using the waypoint index which increments through the array.
                        e.currentWaypoint = e.waypoints[e.waypointIndex];
                        // Updating enemy ai max wait time.
                        e.maxWaitTime = e.currentWaypoint.waitTime;
                        // Move to the waypoint.
                        ai.MoveToPosition(e.currentWaypoint.targetDestination.position);
                    }
                }
            }
        }

        public static void SearchBehaviour(EnemyAi ai)
        {
            // Encapsulation
            EnemyStats e = ai.enemyStats;
            // Check the distance between the ai and the position of the target.
            float distanceCheck = Vector3.Distance(ai.transform.position, e.candidatePosition);

            // Ai has reached the position.
            if(distanceCheck < 2)
            {
                // Using a wait time makes the search behaviour more realistic.
                e.waitTimeWP += Time.deltaTime * 10;

                // Time countdown check, once waitTimeWP goes above maxWaitTime,
                // Reset and move to the next position. 
                if (e.waitTimeWP > e.maxWaitTime)
                {
                    // Restting the timer.
                    e.waitTimeWP = 0;
                    // Creating a random wait time.
                    e.maxWaitTime = Random.Range(3, 6);
                    // Update candidate position to the last known position of target.
                    e.candidatePosition = ai.RandomVector3AroundTarget(e.lastKnownPosition);
                    // Move to the candidate position and search the area.
                    ai.MoveToPosition(ai.enemyStats.candidatePosition);
                }
            }
        }

        public static void ChaseBehaviour(EnemyAi ai)
        {
            // Encapsulation
            EnemyStats e = ai.enemyStats;
            // Check the distance between the ai and the position of the target.
            e.chaseDistanceCheck = Vector3.Distance(ai.transform.position, e.lastKnownPosition);
            

            if (e.chaseDistanceCheck < 5)
            {
                ai.StopMoving();
            }
            else
            {
                ai.MoveToPosition(e.lastKnownPosition);
            }
        }

        public static void GetToCover(EnemyAi ai)
        {
            EnemyStats e = ai.enemyStats;

            // If there is no targetCover.
            if(e.targetCover == null)
            {
                // Find targetCover passing in the position of the AI and the position of the target.
                e.targetCover = AIDirector.singleton.GetCover(ai.transform.position, e.lastKnownPosition);

                // If none exist.
                if(e.targetCover == null)
                {
                    Debug.Log("no target found");
                    ai.ChangeInViewState(EnemyAi.inViewSubCat.inChase);
                }
                else
                {
                    if (AIDirector.singleton.coverPositions.Contains(e.targetCover))
                        AIDirector.singleton.coverPositions.Remove(e.targetCover);
                        // Remove from list so that only one ai can use each cover position.

                    ai.MoveToPosition(e.targetCover.position);
                }
            }
            else
            {
                // Calculate the distance between the AI and the targetted cover position.
                float distanceCheck = Vector3.Distance(ai.transform.position, e.targetCover.position);
                
                // Inside cover.
                if (distanceCheck < 1)
                {
                    ai.StopMoving();
                    // Change state to inCover because AI is inCover.
                    ai.ChangeInViewState(EnemyAi.inViewSubCat.inCover);
                }

                if(e.distanceFromTarget < 5)
                {
                    ai.StopMoving();
                    // Change state to inCover because AI is inCover.
                    ai.ChangeInViewState(EnemyAi.inViewSubCat.inRange);
                }
            }
        }

        public static void ProgressBehaviour(SpyAI ai)
        {
            SpyStats e = ai.spyStats;
            
            // If waypoints exist.
            if (e.waypoints.Count > 0)
            {
                e.firstWaypoint = e.waypoints[0];

                if (e.currentWaypoint.targetDestination == null)
                {
                    e.currentWaypoint = e.waypoints[e.waypointIndex];
                    ai.MoveToPosition(e.currentWaypoint.targetDestination.position);
                }
                if (ai.agent.velocity == Vector3.zero)
                {
                    ai.agent.isStopped = false;
                }
                // Go to waypoint.
                
                e.currentWaypoint = e.waypoints[e.waypointIndex];
                Debug.Log(e.currentWaypoint);
                // Calculating the waypoint distance.
                e.waypointDistance = Vector3.Distance(ai.transform.position, e.currentWaypoint.targetDestination.position);

                if (e.waypointDistance < 2)
                {
                    Debug.Log("waypoint distance: " + e.waypointDistance);
                    e.waitTimeWP += Time.deltaTime * 35; // * 15 cause called on every 15 frames, so offset of 15 to bring it to realtime.

                    if (e.waitTimeWP > e.maxWaitTime)
                    {
                        e.waitTimeWP = 0;
                        // Go to next waypoint or if there isn't another one reverse.
                        if (e.waypointIndex < e.waypoints.Count - 1)
                        {
                            // Increment the index to move to the next waypoint in array.
                            e.waypointIndex++;
                        }
                        else
                        {
                            // Runs when waypointIndex reaches the end.
                            // And then reverses the array to create a never ending loop.
                            e.waypointIndex = 0;
                            e.waypoints.Reverse();
                        }

                        // Updating the current waypoint using the waypoint index which increments through the array.
                        e.currentWaypoint = e.waypoints[e.waypointIndex];
                        // Updating enemy ai max wait time.
                        e.maxWaitTime = e.currentWaypoint.waitTime;
                        Debug.Log("Move to: " + e.currentWaypoint.targetDestination.position);
                        // Move to the waypoint.
                        ai.MoveToPosition(e.currentWaypoint.targetDestination.position);
                    }
                }
            }
        }

        public static void SpyHide(SpyAI ai)
        {
            SpyStats e = ai.spyStats;
            ai.inHide = false;
            // If there is no targetCover.
            if (e.targetHide == null)
            {
                // Find targetCover passing in the position of the AI and the position of the target.
                e.targetHide = AIDirector.singleton.GetHide(ai.transform.position, e.lastKnownPosition);

                // If none exist.
                if (e.targetHide == null)
                {
                    Debug.Log("no target found");
                    ai.ChangeInViewState(SpyAI.inViewSubCat.takeDown);
                }
                else
                {
                    if (AIDirector.singleton.coverPositions.Contains(e.targetHide))
                        AIDirector.singleton.coverPositions.Remove(e.targetHide);
                    // Remove from list so that only one ai can use each cover position.

                    ai.MoveToPosition(e.targetHide.position);
                }
            }
            else
            {
                // Calculate the distance between the AI and the targetted cover position.
                float distanceCheck = Vector3.Distance(ai.transform.position, e.targetHide.position);

                // Inside cover.
                if (distanceCheck < 1)
                {
                    ai.StopMoving();
                    // Change state to inCover because AI is inCover.
                    ai.ChangeInViewState(SpyAI.inViewSubCat.inHide);
                    ai.inHide = true;
                }
                /*
                if (e.distanceFromTarget < 5)
                {
                    ai.ChangeInViewState(SpyAI.inViewSubCat.takeDown);
                }*/
            }
        }
    }

    [System.Serializable]
    public class WaypointsBase
    {
        public Transform targetDestination;
        public float waitTime = 1;
    }  
}
