﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// These are instances so do not assign here.
namespace DG.AI
{
    [System.Serializable]
    public class EnemyStats
    {
        // --- Waypoint Variables ---
        public List<WaypointsBase> waypoints = new List<WaypointsBase>();
        // Current waypoint variable.
        [HideInInspector]
        public WaypointsBase currentWaypoint;
        // Current waypoint index.
        [HideInInspector]
        public int waypointIndex;
        // Distance between the waypoint and the AI.
        [HideInInspector]
        public float waypointDistance;
        // Wait time of the current waypoint.
        [HideInInspector]
        public float waitTimeWP;
        // Max wait time of waypoints.
        [HideInInspector]
        public float maxWaitTime;


        // --- Target variables ---
        // Last known position of the target.
        [HideInInspector]
        public Vector3 lastKnownPosition;
        // Distance from the target.
        [HideInInspector]
        public float distanceFromTarget;

        // Life of the behaviour before it expires.
        //[HideInInspector]
        public float behaviourLife;
        // Variable to check whether behaviour life has reached the max.
        //[HideInInspector]
        public float maxBehaviourLife;

        // --- Search Variables ---
        [HideInInspector]
        public Vector3 candidatePosition;

        // --- Chase Variables ---
        public float inRange;

        // --- Cover Variables ---
        public Transform targetCover;

        public float chaseDistanceCheck;
    }
}






