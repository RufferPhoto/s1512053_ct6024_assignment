﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

// S1512053 Dan Gregg
// This script contains the queues and request pulls for pathfinding.

/// <summary>
/// References
/// Lague, S. (2017). A* Pathfinding Tutorial(Unity) - YouTube. [online] YouTube.Available at: https://www.youtube.com/playlist?list=PLFt_AvWsXl0cq5Umv3pMC9SPnKjfp9eGW [Accessed 1 Jan. 2018].
/// </summary>

public class PathRequestManager : MonoBehaviour
{
    // Variable to store the queue of path requests.
    Queue<PathRequest> pathRequestQueue = new Queue<PathRequest>();
    // Current path request.
    PathRequest currentPathRequest;
    // Static variable to access the static function RequestPath.
    static PathRequestManager instance;
    // Reference to the pathfinding class.
    Pathfinding pathfinding;
    // Check bool for whether or not this script is currently processing a path.
    bool isProcessingPath;

	void Awake()
    {
		instance = this;
        // Getting the pathfinding component.
        pathfinding = GetComponent<Pathfinding>();
	}

	public static void RequestPath(Vector3 pathStart, Vector3 pathEnd, Action<Vector3[], bool> callback)
    {
		PathRequest newRequest = new PathRequest(pathStart,pathEnd,callback);
        // Adding request to the queue.
        instance.pathRequestQueue.Enqueue(newRequest);
        // Process the next waypoint.
		instance.TryProcessNext();
	}

    // This function calculates whether a queue is already being processed.
    // If not then it will ask the pathfinding script to process the next path.
    void TryProcessNext()
    {
        // Check to see whether a path is already being processed and there is atleast one path in the queue.
        if (!isProcessingPath && pathRequestQueue.Count > 0)
        {
            // Gettin the first path in the queue and takes it from the queue.
            currentPathRequest = pathRequestQueue.Dequeue();
            // Currently processing path so change the bool to true.
            isProcessingPath = true;
            // Function called to process the path.
            pathfinding.StartFindPath(currentPathRequest.pathStart, currentPathRequest.pathEnd);
		}
	}

    // Function will be called once this script has finished processing the path.
    public void FinishedProcessingPath(Vector3[] path, bool success)
    {
		currentPathRequest.callback(path,success);
        // Now not processing path.
        isProcessingPath = false;
        // Process the next path.
        TryProcessNext();
	}

    // Struct to hold the path request variables.
	struct PathRequest
    {
		public Vector3 pathStart;
		public Vector3 pathEnd;
		public Action<Vector3[], bool> callback;

		public PathRequest(Vector3 _start, Vector3 _end, Action<Vector3[], bool> _callback)
        {
			pathStart = _start;
			pathEnd = _end;
			callback = _callback;
		}
	}
}
