﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

// S1512053 Dan Gregg CT6024
// A Star pathfinding.

/// <summary>
/// References
/// Lague, S. (2017). A* Pathfinding Tutorial(Unity) - YouTube. [online] YouTube.Available at: https://www.youtube.com/playlist?list=PLFt_AvWsXl0cq5Umv3pMC9SPnKjfp9eGW [Accessed 1 Jan. 2018].
/// </summary>

public class Pathfinding : MonoBehaviour
{
	
	PathRequestManager requestManager;
	Grid grid; // Reference to Grid.
    // Making the waypoints visible to other scripts i.e. AI so they can rotate toward the waypoint.
    public Vector3[] waypoints;


    void Awake()
    {
		requestManager = GetComponent<PathRequestManager>();
		grid = GetComponent<Grid>();
	}
	
	
	public void StartFindPath(Vector3 startPos, Vector3 targetPos)
    {
		StartCoroutine(FindPath(startPos,targetPos));
	}
	
	IEnumerator FindPath(Vector3 startPos, Vector3 targetPos)
    {
        // Vector3 Array
        waypoints = new Vector3[0];
        // Check for whether or not the pathfinding was a success.
        bool pathSuccess = false;

        // Start and Finish variables.
        Node startNode = grid.NodeFromWorldPoint(startPos);
		Node targetNode = grid.NodeFromWorldPoint(targetPos);

        // Check to see if the path is walkable, if not dont execute the following code.
        if (startNode.walkable && targetNode.walkable)
        {
            // Algorithm
            Heap<Node> openSet = new Heap<Node>(grid.MaxSize);
			HashSet<Node> closedSet = new HashSet<Node>();
			openSet.Add(startNode);
			
			while (openSet.Count > 0)
            {
                // Add start node to OPEN.
                // If the two nodes being compared have an equal fCost then calculate which one is closest to the endNode by comparing the hCost.
                // Current = node in OPEN with the lowest fCost.
                // Remove current from OPEN.
                Node currentNode = openSet.RemoveFirst(); // openSet[0] is the start.
                // Add current to CLOSED.
                closedSet.Add(currentNode);

                // If current is the target node then path has been found & return.
                if (currentNode == targetNode)
                {
                    // print("Path found" + sw.ElapsedMilliseconds + " ms");
                    pathSuccess = true;
					break;
				}

                // Foreach neightbour of the current node.
                foreach (Node neighbour in grid.GetNeighbours(currentNode))
                {
                    // If neighbour is not traversable or neighbour is in CLOSED, skip to next neighbour.
                    if (!neighbour.walkable || closedSet.Contains(neighbour))
                    {
						continue;
					}
					
					int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour);
                    // If new path to neighbour is shorter OR neighbour is not in OPEN then set fCost of neighbour & set parent of neighbour to current.
                    if (newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
                    {
						neighbour.gCost = newMovementCostToNeighbour;
						neighbour.hCost = GetDistance(neighbour, targetNode);
						neighbour.parent = currentNode;

                        // If neighbour is not in OPEN add neighbour to OPEN.
                        if (!openSet.Contains(neighbour))
							openSet.Add(neighbour);
					}
				}
			}
		}
		yield return null;
		if (pathSuccess)
        {
			waypoints = RetracePath(startNode,targetNode);
		}
		requestManager.FinishedProcessingPath(waypoints,pathSuccess);	
	}

    // Function to trace back to start.
    Vector3[] RetracePath(Node startNode, Node endNode)
    {
		List<Node> path = new List<Node>();
        // Trace path backwards because thats how the parents work.
        Node currentNode = endNode;

        // Retracing the steps/path until the starting node is reached,
        // This will then generate the path.
        while (currentNode != startNode)
        {
			path.Add(currentNode);
			currentNode = currentNode.parent;
		}
		Vector3[] waypoints = SimplifyPath(path);
        // Setting the path the correct way around.
        Array.Reverse(waypoints);
		return waypoints;
		
	}

    // Function to simplify the path.
    Vector3[] SimplifyPath(List<Node> path)
    {
        // List of the waypoints.
        List<Vector3> waypoints = new List<Vector3>();
        // Vector2 to store the direction of the last two nodes.
        Vector2 directionOld = Vector2.zero;
		
		for (int i = 1; i < path.Count; i ++)
        {
			Vector2 directionNew = new Vector2(path[i-1].gridX - path[i].gridX,path[i-1].gridY - path[i].gridY);
            // Path has changed direction.
            if (directionNew != directionOld)
            {
                // Add waypoint to waypoints list.
                waypoints.Add(path[i].worldPosition);
			}
			directionOld = directionNew;
		}
		return waypoints.ToArray();
	}

    // Get distance function.
    int GetDistance(Node nodeA, Node nodeB)
    {
		int dstX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
		int dstY = Mathf.Abs(nodeA.gridY - nodeB.gridY);
		
		if (dstX > dstY)
			return 14*dstY + 10* (dstX-dstY);
		return 14*dstX + 10 * (dstY-dstX);
	}	
}
