﻿using UnityEngine;
using System.Collections;

public class Unit : MonoBehaviour
{

    // Basic class variables.
	public Transform target;
	public float speed = 20;
	Vector3[] path;
	int targetIndex;

    
	void Start()
    {
        // Find a new path.
        PathRequestManager.RequestPath(transform.position,target.position, OnPathFound);
	}

    // Function to run path once one is found.
	public void OnPathFound(Vector3[] newPath, bool pathSuccessful)
    {
        // No point running if not successful.
		if (pathSuccessful)
        {
			path = newPath;
            // Start index at 0.
			targetIndex = 0;
			StopCoroutine("FollowPath");
			StartCoroutine("FollowPath");
		}
	}

	IEnumerator FollowPath()
    {
        // First waypoint is first entry in array.
		Vector3 currentWaypoint = path[0];
		while (true)
        {
            // When enemy AI reaches the current way point execute.
			if (transform.position == currentWaypoint)
            {
                // Increment targetIndex. 
				targetIndex++;
                // Break out IEnumerator once the goal has been reached.
				if (targetIndex >= path.Length)
                {
					yield break;
				}
                // Current waypoint will equal the current path.
				currentWaypoint = path[targetIndex];
			}
            // Move towards the goal.
			transform.position = Vector3.MoveTowards(transform.position,currentWaypoint,speed * Time.deltaTime);
			yield return null;
		}
	}

    // Debug visual display.
	public void OnDrawGizmos()
    {
		if (path != null) {
			for (int i = targetIndex; i < path.Length; i ++)
            {
				Gizmos.color = Color.black;
				Gizmos.DrawCube(path[i], Vector3.one);

				if (i == targetIndex)
                {
					Gizmos.DrawLine(transform.position, path[i]);
				}
				else
                {
					Gizmos.DrawLine(path[i-1],path[i]);
				}
			}
		}
	}
}
