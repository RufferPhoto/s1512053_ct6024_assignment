﻿using UnityEngine;
using System.Collections;

// S1512053 Dan Gregg CT6024
// Node class.

/// <summary>
/// References
/// Lague, S. (2017). A* Pathfinding Tutorial(Unity) - YouTube. [online] YouTube.Available at: https://www.youtube.com/playlist?list=PLFt_AvWsXl0cq5Umv3pMC9SPnKjfp9eGW [Accessed 1 Jan. 2018].
/// </summary>

public class Node : IHeapItem<Node>
{
	
	public bool walkable;
	public Vector3 worldPosition;
	public int gridX;
	public int gridY;

	public int gCost;
	public int hCost;
	public Node parent;
	int heapIndex;
	
	public Node(bool _walkable, Vector3 _worldPos, int _gridX, int _gridY)
    {
		walkable = _walkable;
		worldPosition = _worldPos;
		gridX = _gridX;
		gridY = _gridY;
	}

	public int fCost // fCost is always equal to gCost + hCost.
                     // It is never needed to assign to fCost so only a get function is required.
    {
        get
        {
			return gCost + hCost;
		}
	}

    // Implement everything specified in the IHeapItem interface
    public int HeapIndex
    {
		get
        {
			return heapIndex;
		}
		set
        {
			heapIndex = value;
		}
	}

    // Function to compare
    public int CompareTo(Node nodeToCompare)
    {
        // Compare the fCosts of the two nodes.
        int compare = fCost.CompareTo(nodeToCompare.fCost);
        // If two fCosts are equal.
        if (compare == 0)
        {
            // Checks the hCosts to see which one has higher priority.
            compare = hCost.CompareTo(nodeToCompare.hCost);
		}
        // Return one if the current item has a higher priority than the item it is being compared to, however with integers it will return one if the integer is higher so need to reverse it with nodes
        // Need to return one if it is lower.
        return -compare;
	}
}
