﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Grid : MonoBehaviour
{

	public bool displayGridGizmos;
	public LayerMask unwalkableMask;
	public Vector2 gridWorldSize; // Define the area in world coordinates that the grid will cover.
    public float nodeRadius; // To define how much individual space each node covers.
    Node[,] grid; // 2D array of nodes which will represent the grid.

    float nodeDiameter;
	int gridSizeX, gridSizeY;

	void Awake()
    {
		nodeDiameter = nodeRadius*2;
		gridSizeX = Mathf.RoundToInt(gridWorldSize.x/nodeDiameter);
		gridSizeY = Mathf.RoundToInt(gridWorldSize.y/nodeDiameter);
		CreateGrid();
	}

	public int MaxSize
    {
		get
        {
			return gridSizeX * gridSizeY;
		}
	}

	void CreateGrid()
    {
		grid = new Node[gridSizeX,gridSizeY];
        // Calculation for the bottom left corner of the grid.
        Vector3 worldBottomLeft = transform.position - Vector3.right * gridWorldSize.x/2 - Vector3.forward * gridWorldSize.y/2;
        
        // Loop for collision check
        // As x increases increments of nodeDiamter until reaches the edge.
        for (int x = 0; x < gridSizeX; x ++)
        {
			for (int y = 0; y < gridSizeY; y ++)
            {
				Vector3 worldPoint = worldBottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.forward * (y * nodeDiameter + nodeRadius);
                // Collision check
                bool walkable = !(Physics.CheckSphere(worldPoint,nodeRadius,unwalkableMask));
				grid[x,y] = new Node(walkable,worldPoint, x,y);
			}
		}
	}

	public List<Node> GetNeighbours(Node node)
    {
		List<Node> neighbours = new List<Node>();

		for (int x = -1; x <= 1; x++)
        {
			for (int y = -1; y <= 1; y++)
            {
				if (x == 0 && y == 0)
					continue;

				int checkX = node.gridX + x;
				int checkY = node.gridY + y;

				if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY)
                {
					neighbours.Add(grid[checkX,checkY]);
				}
			}
		}
		return neighbours;
	}

    // Function to find the node that an object is standing on i.e player.
    // Returns a percentage between 0 and 1
    public Node NodeFromWorldPoint(Vector3 worldPosition)
    {
		float percentX = (worldPosition.x + gridWorldSize.x/2) / gridWorldSize.x;
		float percentY = (worldPosition.z + gridWorldSize.y/2) / gridWorldSize.y;
        // Clamping so that the result can only be between 0 and 1.
        percentX = Mathf.Clamp01(percentX);
		percentY = Mathf.Clamp01(percentY);
        // Getting the x and y indeces of the 2D grid array.
        // -1 because i.e if on right of the grid and x = 1 then you still need to subtract one so that it isn't outside the bounds of the array.
        int x = Mathf.RoundToInt((gridSizeX-1) * percentX);
		int y = Mathf.RoundToInt((gridSizeY-1) * percentY);
		return grid[x,y];
	}
	
    // Debug visual
	void OnDrawGizmos()
    {
		Gizmos.DrawWireCube(transform.position,new Vector3(gridWorldSize.x,1,gridWorldSize.y));
		if (grid != null && displayGridGizmos)
        {
			foreach (Node n in grid)
            {
				Gizmos.color = (n.walkable)?Color.white:Color.red;
				Gizmos.DrawCube(n.worldPosition, Vector3.one * (nodeDiameter-.1f));
			}
		}
	}
}