﻿using UnityEngine;
using System.Collections;
using System;

// This script optimises the A* Pathfinding algorithm.
// Generic so that it can work with other classes.

public class Heap<T> where T : IHeapItem<T>
{
	
	T[] items;
	int currentItemCount;
	
	public Heap(int maxHeapSize)
    {
		items = new T[maxHeapSize];
	}
	
	public void Add(T item)
    {
		item.HeapIndex = currentItemCount;
        // Adding the items to the end of the array. However not necessarilly where it belongs so it needs to be compared with parent.
        items[currentItemCount] = item;
        // SortUp function will handle the sorting as mentioned above.
        SortUp(item);
		currentItemCount++;
	}

    // Removing first item from the heap function.
    public T RemoveFirst()
    {
        // Saving the firstItem.
        T firstItem = items[0];
        // One less item on the heap.
        currentItemCount--;
        // Take the item at the end of the heap and put it into the first place.
        items[0] = items[currentItemCount];
		items[0].HeapIndex = 0;
		SortDown(items[0]);
		return firstItem;
	}

    // Function to change the priority of an item in pathfinding where you may find a node in the openSet which needs to be updated with a lower fCost, because it has a new path to it.
    // It will need its node updating in the heap for example.
    public void UpdateItem(T item)
    {
        // In pathfinding items will only have their priority increased and never decreased so SortDown wont need to be called.
        // However the function may be used in a different scenario which then could be added here.
        SortUp(item);
	}

    // Current number of items in the heap
    public int Count
    {
		get
        {
			return currentItemCount;
		}
	}

    // Function to check whether the heap contains a specific item.
    public bool Contains(T item)
    {
        // Using equals mehod to check whether two items are equal.
        // If they are equal it returns true, if not then returns false.
        return Equals(items[item.HeapIndex], item);
	}

	void SortDown(T item)
    {
		while (true)
        {
            // Get indeces of the items two children.
            int childIndexLeft = item.HeapIndex * 2 + 1;
			int childIndexRight = item.HeapIndex * 2 + 2;
			int swapIndex = 0;

            // Check to see if this item does atleast have one child
            if (childIndexLeft < currentItemCount)
            {
				swapIndex = childIndexLeft;
                // Check to see if this item also has a child on the right
                if (childIndexRight < currentItemCount)
                {
                    // Check to see which of the children has a higher priority.
                    if (items[childIndexLeft].CompareTo(items[childIndexRight]) < 0)
                    {
                        // Set the swap index to that child.
                        swapIndex = childIndexRight;
                        // Swap index is now equal to the child with the highest priority.
                    }
                }

                // Check if the parent has a lower priority than it's highest priority child in which case they will be swapped.
                if (item.CompareTo(items[swapIndex]) < 0)
                {
					Swap (item,items[swapIndex]);
				}
                // If the parent has a higer priority than both of its children then it's in its correct position then return can be called to exit from the loop.
                else
                {
					return;
				}
			}
            // If the parent doesn't have any children then it is also in it's correct position and return can be called to exit from the loop.
            else
            {
				return;
			}

		}
	}
	
	void SortUp(T item)
    {
		int parentIndex = (item.HeapIndex-1)/2;
		
		while (true)
        {
			T parentItem = items[parentIndex];
            // If the compareTo has a higher priority then it returns 1, if it has the same then it returns 0, And if it has a lower priority then it returns -1.
            // I.e. if the item has a higher priority than the parent item, which means lower fCost then it should be swapped with the parent item.
            if (item.CompareTo(parentItem) > 0)
            {
				Swap (item,parentItem);
			}
            // As soon as it is no longer of a higher priority than the parent item then break out the loop.
            // Otherwise it will keep recalculating the parentIndex and comparing the item to it's new parent.
            else
            {
				break;
			}
			parentIndex = (item.HeapIndex-1)/2;
		}
	}

    // Swap item function.
    void Swap(T itemA, T itemB)
    {
		items[itemA.HeapIndex] = itemB;
		items[itemB.HeapIndex] = itemA;
        // Swap heapIndex values.
        int itemAIndex = itemA.HeapIndex;
		itemA.HeapIndex = itemB.HeapIndex;
		itemB.HeapIndex = itemAIndex;
	}
}

// Each item needs to be able to keep track of its own index in the heap.
// Also comparing two items, so can figure out which has the higher priority so it can be sorted in the heap.
// In order to do this we require an interface.

public interface IHeapItem<T> : IComparable<T>
{
	int HeapIndex
    {
		get;
		set;
	}
}
