﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// S1512053 Dan Gregg CT6024 Advanced AI Algorithms
// Spy AI Class.

namespace DG.AI
{
    public class SpyAI : MonoBehaviour
    {
        public List<EnemyAi> enemyTargets = new List<EnemyAi>();
        public EnemyAi enemyTarget;
        Vector3 direction;
        Vector3 rotDirection;

        Quaternion startingAngle = Quaternion.AngleAxis(-60, Vector3.up);
        Quaternion stepAngle = Quaternion.AngleAxis(5, Vector3.up);

        // Reference to the waypoints the Spy will use to guide to the goal.
        public Vector3[] WPRotDirection;

        // Field of view for the Spy.
        public float FOVAngle;
        // Vision radius.
        public float radius = 10;
        // Speed of the Spy.
        public float speed = 5;
        // Max distance before going into lateBehaviour.

        // Delayed frame / Late behaviour counter.
        // Optimisation so that some functions only run on certain frames and not everyframe.
        int i_lateFrame = 10;
        int i_lateFrameCounter = 0;

        int i_llateFrame = 35;
        int i_llateFrameCounter = 0;

        // Private
        // This prevents physics running when not needed. i.e when spy is on other side of the map,
        // without this unity's physics engine will still be called and increase cpu/gpu usage.
        private bool _isInView;
        private bool _isInAngle;
        private bool _isClear;
        public bool inHide;

        public SpyState spyState;
        public SpyState targetState;
        public inViewSubCat inViewSub;

        // Delegates to be used as a variable to be passed around functions or states.
        delegate void EveryFrame();
        EveryFrame everyFrame;
        delegate void LateFrame();
        LateFrame lateFrame;
        delegate void LateLateFrame();
        LateLateFrame llateFrame;

        // Reference to navmesh agent.
        [HideInInspector]
        public NavMeshAgent agent;
        // Instantiating Spy Stats for this Spy.
        public SpyStats spyStats = new SpyStats();

        // Debugging 
        public bool changeState;

        void Start()
        {
            agent = GetComponent<NavMeshAgent>();
            // Forcing spy into moving state at the begining.
            spyState = SpyState.moving;
            ChangeState(SpyState.moving);
            // Adding all enemies to the enemyTargets list.
            enemyTargets = AIDirector.singleton.allEnemies;

            for (int i = 0; i < enemyTargets.Count; i++)
            {
                enemyTargets[i].GetComponent<EnemyAi>();
            }
        }

        void Update()
        {
           if (changeState)
            {
                ChangeState(targetState);
                changeState = false;
            }

            MonitorStates();

            // Runs everyframe;
            if (everyFrame != null)
                everyFrame();
            i_lateFrameCounter++;

            if (i_lateFrameCounter > i_lateFrame)
            {
                if (lateFrame != null)
                {
                    lateFrame();
                }
                i_lateFrameCounter = 0;
            }

            i_llateFrameCounter++;
            if (i_llateFrameCounter > i_llateFrame)
            {
                if (llateFrame != null)

                    llateFrame();
                i_llateFrameCounter = 0;
            }
        }

        void MonitorStates() // Runs on everyframe.
        {
            switch (spyState)
            {
                case SpyState.wait:
                    if (!enemyTarget._isClear)
                        ChangeState(SpyState.moving);
                    break;
                case SpyState.moving:
                    if (enemyTarget._isClear && !inHide)
                    {
                        Debug.Log("look for hide");
                        ChangeState(SpyState.inView);
                        ChangeInViewState(inViewSubCat.lookForHide);
                    }
                    break;
                case SpyState.inView:
                    break;
                default:
                    break;
            }
        }

       
        public void ChangeState(SpyState targetState)
        {
            spyState = targetState;
            DetectEnemies();
            // Empty all the delegates when change state occurs so that the 
            // AI doesn't run the previous set state.
            everyFrame = null;
            lateFrame = null;
            llateFrame = null;

            switch (targetState)
            {
                case SpyState.wait:
                    StopMoving();
                    break;
                case SpyState.moving:
                    // Doesn't need to be every frame. Reducing CPU usage.
                    lateFrame = MovingBehaviour;
                    break;
                case SpyState.inView:
                    everyFrame = InViewBehaviour;
                    break;
                default:
                    break;
            }
        }

        void InViewBehaviourSec()
        {
            Sight();
            MonitorTargetPosition();
        }

        // Function is called when the AI is inView of the target but needs a behaviour i.e. chase the Spy.
        public void ChangeInViewState(inViewSubCat target)
        {
            inViewSub = target;
        }

        void MovingBehaviour()
        {
            if (enemyTarget == null)
                return;
            
            Sight();
            CommonBehaviours.ProgressBehaviour(this);
        }

        void InViewBehaviour()
        {
            if (enemyTargets == null)
                return;
            FindDirection(enemyTarget.transform);
            DistanceCheckEnemy(enemyTarget.transform);

            switch (inViewSub)
            {
                case inViewSubCat.inRange:
                    break;
                case inViewSubCat.takeDown:
                    break;
                case inViewSubCat.lookForHide:
                    CommonBehaviours.SpyHide(this);
                    break;
                case inViewSubCat.inHide:
                    if ((!enemyTarget._isClear) && enemyTarget.enemyStats.behaviourLife > enemyTarget.enemyStats.maxBehaviourLife - 1)
                    {
                        Debug.Log("isClear");
                        WaypointsBase wp = new WaypointsBase();
                        spyStats.currentWaypoint = wp;
                        
                        ChangeState(SpyState.moving);
                        ChangeInViewState(inViewSubCat.inRange);
                    }
                    inHide = true;
                    RotateTowardsTarget();
                    break;
                default:
                    break;
            }
        }

        void Sight()
        {
            // Find distance first.
            DistanceCheckEnemy(enemyTarget.transform);
            // Find the direction.
            FindDirection(enemyTarget.transform);
            // From the direction the angle can then be calculated.
            AngleCheck();
            // Are there any objects in the way of the view. 
            if (_isInAngle)
                IsClearView(enemyTarget.transform);    
        }

        // Function to detect enemies. Runs quite costly so should be used sparingly.
        void DetectEnemies()
        {
            RaycastHit hit;
            Quaternion angle = transform.rotation * startingAngle;
            direction = angle * Vector3.forward;
            Vector3 origin = transform.position;
            
            for (var i = 0; i < 24; i++)
            {
                if (Physics.Raycast(origin, direction, out hit, radius))
                {
                    if (hit.transform.CompareTag("Enemy"))
                    {
                        Debug.DrawRay(origin, direction * radius);
                        Debug.Log("hit enemy");
                        enemyTarget = hit.collider.GetComponent<EnemyAi>();
                        // Not clear to progress.
                        _isClear = false;
                    }
                }
                direction = stepAngle * direction;
            }
        }

        void IsClearView(Transform target)
        {
            // Everytime this isClear is false until the raycast hits the enemy.
            _isClear = false;
            // Raycasting to check whether the player is behind an object.
            RaycastHit hit;
            // Origin of Enemy AI.
            Vector3 origin = transform.position;

            // Drawing line to the target.
            Debug.DrawRay(origin, direction * radius);

            // If hit
            if (Physics.Raycast(origin, direction, out hit, radius))
            {
                if (hit.transform.CompareTag("Enemy"))
                {
                    _isClear = true;
                }
            }
        }

        void AngleCheck()
        {
            Vector3 rotDirection = direction;
            // Force Y = 0.
            rotDirection.y = 0;

            // Default if direction position is null vector.
            if (rotDirection == Vector3.zero)
                rotDirection = transform.forward;

            // Calculate whether or not the enemy is within Spy FOV.
            float f_angle = Vector3.Angle(transform.forward, rotDirection);
            _isInAngle = (f_angle < FOVAngle);
        }

        void RotateTowardsTarget()
        {
            if (rotDirection == Vector3.zero)
                rotDirection = transform.forward;

            // Calculating rotation to target.
            Quaternion targetRotation = Quaternion.LookRotation(rotDirection);
            // Applying
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 5);
        }

        void FindDirection(Transform target)
        {
            direction = target.position - transform.position;
            rotDirection = direction;
            rotDirection.y = 0;
        }
       
        void DistanceCheckEnemy(Transform target)
        {
            spyStats.distanceFromTarget = Vector3.Distance(transform.position, target.position);
        }

        void MonitorTargetPosition()
        {
            float delta_distance = Vector3.Distance(transform.position, spyStats.lastKnownPosition);
            if (delta_distance > 2)
            {
                spyStats.lastKnownPosition = enemyTarget.transform.position;
            }
        }

        public void MoveToPosition(Vector3 targetPos)
        {
            agent.isStopped = false;
            // Function to move to a target.
            agent.SetDestination(targetPos);
        }

        // Simple function to stop the Spy.
        public void StopMoving()
        {
            agent.isStopped = true;
        }
        
        public enum SpyState
        {
            wait, moving, inView 
        }

        public enum inViewSubCat
        {
            inRange, takeDown, lookForHide, inHide
        }
    }
}