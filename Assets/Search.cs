﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Search : MonoBehaviour {
    

    // Global Static
    public static Vector3 s_vSearchCentrePosition;

    // Const
    public const float fSEARCH_RADIUS = 20.0f;
    public const float fMAX_WAIT_TIME = 1.5f;

    // Variable Declaraions
    NavMeshAgent xNavMeshAgent;
    Vector3 vCurrentSearchPosition;
    float fWaitTimer = 0.0f;
    bool bSearchPositionFound = false;

    // Use this for initialization
    void Start ()
    {
        // Get Reference to navmeshagent;
        xNavMeshAgent = GetComponent<NavMeshAgent>();
        s_vSearchCentrePosition = transform.position;
	}
	
	// Update is called once per frame
	void Update ()
    {
        // Run if SearchPosition vector is not = to a blank vector.
	    if(!bSearchPositionFound && s_vSearchCentrePosition != new Vector3())
        {
            vCurrentSearchPosition = s_vSearchCentrePosition = (Random.insideUnitSphere * fSEARCH_RADIUS);
            NavMeshHit xHitResult;

            // Give position that is given as current search value, then hitresult, max distance and then all areas.
            // if true then position is okay if false then it will give a new position.
            if(NavMesh.SamplePosition(vCurrentSearchPosition, out xHitResult, 1.0f, NavMesh.AllAreas))
            {
                // Update with new position.
                vCurrentSearchPosition = xHitResult.position;
                // Set destination on the nav mesh.
                xNavMeshAgent.SetDestination(vCurrentSearchPosition);
                // Setting the wait timer.
                fWaitTimer = fMAX_WAIT_TIME;
                // Found search position.
                bSearchPositionFound = true;

            }
            else
            {
                // If close to destination of 0.5f
                if (xNavMeshAgent.remainingDistance < 0.5f)
                {
                    if (fWaitTimer > 0.0f)
                    {
                        fWaitTimer -= Time.deltaTime;
                    }
                    else
                    {
                        bSearchPositionFound = false;
                        vCurrentSearchPosition = new Vector3();
                    }
                }
            }
        }	
	}
}
